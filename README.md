# CI-hdl

This projects provides some docker script files and instructions to create a docker image with Linux and Quartus which can be used to create build machines for Altera FPGA projects in continuous integration work flows.

## Getting Started

Intel FPGAs use the Quartus Prime software for synthesis and on Linux, the supported OS is Red Hat Enterprise Linux. It is possible to run Quartus Prime on Ubuntu too with some extra steps, but this is not supported by Intel.

This repository contains several docker images in its registry which can be used to compile or simulate hdl projects.
In particular, it contains the following images: 
- Ubuntu_base: Is an image based on Ubuntu 16.04 whhere the required libraries to install Quartus or ModelSim have been added.
- quartus-15_1: Is an image build on top of the precedent with Altera Quartus standard 15.1. 
- modelsim_altera_17-1: Is an image build on top of the ubuntu_base image with ModelSim installed.
- vunit: Is an image build on top of modelsim_altera_17-1 with Vunit python module installed.

Please, note that Quartus and ModelSim downloads are very large, so they are not kept in this repository. They are nevertheless required to customize/modify or rebuild the docker images contained in this repo registry. If you plan to rebuild the images, you need to donwload them and place the local files in the same folder than the docker build scripts.

### Prerequisites

There are several  ways of using this repository. 

1) The most easy way is to setup the `.gitlab-ci.yml` file of your project repository to point to the  quartus docker image available in this repository registry. In order to do that it is enought to add the following line at the begining of your continuous integration hdl job. Use the appropiate image depending if you wish to compile the hdl design, simulate it, etc. 

```yaml
image: gitlab-registry.cern.ch/ecalvo/ci-hdl/quartus-15_1
```

This use case does not require any additional software to be installed. 

2) If you wish to modify the docker image, it will be necessary to install docker CE in your machine. 
Detailed instructions for doing this can be found [here](https://docs.docker.com/glossary/?term=installation "here"). 

3) Once docker CE installed, you can copy the docker build script and modify as you wish. You will then have to push the new docker image to the registry of your own project.

## Acknowledgments

* I would like to thank the authors of the following websites which habe been very useful in the construction of the images available on this repository.
- https://www.jamieiles.com/posts/quartus-docker/
- https://github.com/cdsteinkuehler/QuartusBuildVMs

